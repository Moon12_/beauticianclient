package com.mtechsoft.beaticianapp.fragments.stylishFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.ConfirmedApointmentsAdapter;
import com.mtechsoft.beaticianapp.adapter.styllishSides.ClientHistoryAdapter;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

public class HistoryAppointmentFragment extends Fragment {
//TextView tvTitles;
    View view;
    private RecyclerView recyclerView;
    private ClientHistoryAdapter pAdapter;
    private ArrayList<MemberListModel> memberListModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_posts_stylish, container, false);
        recyclerView=view.findViewById(R.id.rvMmberList);
//        tvTitles=view.findViewById(R.id.tvTitles);
//        tvTitles.setVisibility(View.GONE);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        memberListModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            MemberListModel allPostsTabModel = new MemberListModel();
//            allPostsTabModel.setUserName("Sheikh" + i);
            allPostsTabModel.setPages(10);
            memberListModels.add(allPostsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new ClientHistoryAdapter(getActivity(), memberListModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}