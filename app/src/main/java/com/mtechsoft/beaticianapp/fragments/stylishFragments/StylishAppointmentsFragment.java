package com.mtechsoft.beaticianapp.fragments.stylishFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.AppointmentActivity;
import com.mtechsoft.beaticianapp.adapter.MemberListAdapter;
import com.mtechsoft.beaticianapp.fragments.appointments.BookingApointmentsFragment;
import com.mtechsoft.beaticianapp.fragments.appointments.ConfirmedAppointmentFragment;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;
import java.util.List;

public class StylishAppointmentsFragment extends Fragment {
    ImageView ivBack;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.stylish_appointments_fragments, container, false);

        viewPager =view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout =view. findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
    public void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new UpCommingAppointmentFragment(), "UpComming");
        adapter.addFragment(new StylishConfirmedAppointmentFragment(), "Confirmed");
        adapter.addFragment(new HistoryAppointmentFragment(), "History");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}