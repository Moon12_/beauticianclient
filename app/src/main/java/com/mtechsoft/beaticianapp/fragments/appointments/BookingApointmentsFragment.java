package com.mtechsoft.beaticianapp.fragments.appointments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.Utilities;
import com.mtechsoft.beaticianapp.activites.LocationActivity;
import com.mtechsoft.beaticianapp.adapter.StylishPostAdapter;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BookingApointmentsFragment extends Fragment {

    View view;
    RelativeLayout rlLoCATION;
    TextView location;
    CheckBox cbPPe;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.booking_confirmation, container, false);
//        tvDatePicker = view.findViewById(R.id.tvDatePicker);
        rlLoCATION = view.findViewById(R.id.rlLoCATION);
        location = view.findViewById(R.id.location);
        cbPPe = view.findViewById(R.id.cbPPe);
        cbPPe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog1();
            }
        });
        String getAddress = Utilities.getString(getActivity(), "getAddress");
        location.setText(getAddress);
        rlLoCATION.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LocationActivity.class));
            }
        });
//        tvDatePicker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new SingleDateAndTimePickerDialog.Builder(getActivity())
//                        .bottomSheet()
//                        .curved()
//                        .titleTextColor(Color.RED).backgroundColor(Color.WHITE).mainColor(Color.RED)
////                        .stepSizeMinutes(15)
////                        .displayHours(false)
////                        .displayMinutes(false)
//                        //.todayText("aujourd'hui")
//                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
//                            @Override
//                            public void onDisplayed(SingleDateAndTimePicker picker) {
//                                // Retrieve the SingleDateAndTimePicker
//                            }
////
////                            @Override
////                            public void onClosed(SingleDateAndTimePicker picker) {
////                                // On dialog closed
////                            }
//                        })
//                        .title("Date and Time")
//                        .listener(new SingleDateAndTimePickerDialog.Listener() {
//                            @Override
//                            public void onDateSelected(Date date) {
//
////                                getFormattedDate(date);
//                                tvDatePicker.setText(getFormattedDate(date));
//                            }
//                        }).display();
//            }
//        });
        return view;
    }

    private String getFormattedDate(Date timeZone) {

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-YYYY HH:mm aa");
        String formattedDate = df.format(timeZone);
        return formattedDate;

    }

    @Override
    public void onResume() {
        super.onResume();
        String getAddress = Utilities.getString(getActivity(), "getAddress");
        location.setText(getAddress);
    }

    private void showCustomDialog1() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Dear customer, kindly check the PPE check list of stylist before availing the service \n" +
                "Mask\n" +
                "Gloves\n" +
                "Hand Sanitizer\n" +
                "Disinfectants");
        builder1.setTitle("MObiStylez");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        startActivity(new Intent(MainActivity.this, CustomerRegisterActivity.class));

                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }
}