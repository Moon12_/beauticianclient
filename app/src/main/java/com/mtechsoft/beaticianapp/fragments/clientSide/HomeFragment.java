package com.mtechsoft.beaticianapp.fragments.clientSide;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.MemberListAdapter;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    View view;
    private RecyclerView recyclerView;
    private MemberListAdapter pAdapter;
    private ArrayList<MemberListModel> memberListModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView=view.findViewById(R.id.rvMmberList);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        memberListModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            MemberListModel allPostsTabModel = new MemberListModel();
            allPostsTabModel.setUserName("Sheikh" + i);
            allPostsTabModel.setPages(10);
            memberListModels.add(allPostsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new MemberListAdapter(getActivity(), memberListModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}