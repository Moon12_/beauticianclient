package com.mtechsoft.beaticianapp.fragments.stylishFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.MemberListAdapter;
import com.mtechsoft.beaticianapp.adapter.styllishSides.AllOptionsAdapter;
import com.mtechsoft.beaticianapp.models.AllOptionsListModel;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

public class AllOptionsFragment extends Fragment {

    View view;
    private RecyclerView rvCategory;
    private AllOptionsAdapter pAdapter;
    private ArrayList<AllOptionsListModel> allOptionsListModels;
    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_options, container, false);
        rvCategory = view.findViewById(R.id.rvAloptions);
        allOptionsListModels = new ArrayList<>();
        rvCategory.setHasFixedSize(true);
        linearLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        rvCategory.setLayoutManager(linearLayoutManager);
        allOptionsListModels.add(new AllOptionsListModel("Posts "));
        allOptionsListModels.add(new AllOptionsListModel("Services"));
        allOptionsListModels.add(new AllOptionsListModel("Available Slots"));
        allOptionsListModels.add(new AllOptionsListModel("Set Availability"));
        allOptionsListModels.add(new AllOptionsListModel("Client Request"));
        allOptionsListModels.add(new AllOptionsListModel("Appoint\n-ments"));
//        allOptionsListModels.add(new AllOptionsListModel("Upcoming  Appointment"));
//        allOptionsListModels.add(new AllOptionsListModel("Appointment History"));
//        allOptionsListModels.add(new AllOptionsListModel("Clients Payment"));

        pAdapter = new AllOptionsAdapter(getContext(), allOptionsListModels);

        rvCategory.setAdapter(pAdapter);


        return view;
    }


}