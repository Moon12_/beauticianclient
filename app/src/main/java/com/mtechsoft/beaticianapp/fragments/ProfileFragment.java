package com.mtechsoft.beaticianapp.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.NotificationActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProfileFragment extends Fragment {

    View view;
    ImageView ivNotification;
    RelativeLayout carddeatil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ivNotification=view.findViewById(R.id.ivNotification);
        carddeatil=view.findViewById(R.id.bCard);
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });
        carddeatil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogue();
                Toast.makeText(getContext(), "Card Detail", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void showdialogue() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_dialouge_card_detail);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.60)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        RadioGroup group = dialog.findViewById(R.id.radio);
        TextInputEditText editText_Name = dialog.findViewById(R.id.edit_Card_Name);
        TextInputEditText editText_Number = dialog.findViewById(R.id.edit_Card_Number);
        EditText tvDatePicker = dialog.findViewById(R.id.expeirationDate);
        TextView tv_Update = dialog.findViewById(R.id.bUpdateCard);
//        tvDatePicker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new SingleDateAndTimePickerDialog.Builder(getActivity())
//                        .bottomSheet()
//                        .curved()
//                        .titleTextColor(Color.RED).backgroundColor(Color.WHITE).mainColor(Color.RED)
////                        .stepSizeMinutes(15)
//                        .displayHours(false)
//                        .displayMinutes(false)
//                        //.todayText("aujourd'hui")
//                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
//                            @Override
//                            public void onDisplayed(SingleDateAndTimePicker picker) {
//                                // Retrieve the SingleDateAndTimePicker
//                            }
////
////                            @Override
////                            public void onClosed(SingleDateAndTimePicker picker) {
////                                // On dialog closed
////                            }
//                        })
//                        .title("Date and Time")
//                        .listener(new SingleDateAndTimePickerDialog.Listener() {
//                            @Override
//                            public void onDateSelected(Date date) {
//
////                                getFormattedDate(date);
//                                tvDatePicker.setText(getFormattedDate(date));
//                            }
//                        }).display();
//            }
//        });
        dialog.show();
    }
    private String getFormattedDate(Date timeZone) {

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-YYYY HH:mm aa");
        String formattedDate = df.format(timeZone);
        return formattedDate;

    }
}
