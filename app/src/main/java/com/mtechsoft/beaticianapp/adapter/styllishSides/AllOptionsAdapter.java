package com.mtechsoft.beaticianapp.adapter.styllishSides;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.AppointmentActivity;
import com.mtechsoft.beaticianapp.activites.StylishMainActivity;
import com.mtechsoft.beaticianapp.models.AllOptionsListModel;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;


public class AllOptionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<AllOptionsListModel> allOptionsListModels;

    public AllOptionsAdapter(Context context, ArrayList<AllOptionsListModel> allOptionsListModels) {
        this.context = context;
        this.allOptionsListModels = allOptionsListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_options, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
        holder1.tvTitle.setText(allOptionsListModels.get(position).getTitle());
        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allOptionsListModels.get(position).getTitle().equals("Appointments")){
                    ((StylishMainActivity)context).navController.navigate(R.id.action_allOptionsFragment_to_stylishAppointmentsFragment);

                }
                else if (allOptionsListModels.get(position).getTitle().equals("Client Request")){
                    ((StylishMainActivity)context).navController.navigate(R.id.action_allOptionsFragment_to_clienReqestFragment);


                }

                    Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return allOptionsListModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);

        }

        private void bind(int pos) {
            AllOptionsListModel messagesTabModel = allOptionsListModels.get(pos);
//            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
    }
}

//    public interface Callback {
//        void onItemClick(int pos);
//    }

