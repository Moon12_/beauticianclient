package com.mtechsoft.beaticianapp.adapter.styllishSides;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.MemberProfileActivity;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;


public class HomePostsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<MemberListModel> memberListModels;

    public HomePostsListAdapter(Context context, ArrayList<MemberListModel> memberListModels) {
        this.context = context;
        this.memberListModels = memberListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_beautician_list, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);

        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MemberProfileActivity.class));
            }
        });
        holder1.imbPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, holder1.imbPopUp);
                //inflating menu from xml resource
                popup.inflate(R.menu.more_option_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete:
                                Toast.makeText(context, "Action Performed", Toast.LENGTH_SHORT).show();
//                                MyDatabase database = Room.databaseBuilder(context, MyDatabase.class, "MyCart").allowMainThreadQueries().build();
//                                int id = list.get(position).getId();
//                                database.cartDao().deleteItem(id);
//                                list.remove(position);
//                                notifyDataSetChanged();

                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return memberListModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvAddress, tvTitle;
        ImageView imbPopUp;
        LinearLayout llITem, llShare;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddress = itemView.findViewById(R.id.tvCity);
            imbPopUp = itemView.findViewById(R.id.imbPopUp);

        }

        private void bind(int pos) {
            MemberListModel messagesTabModel = memberListModels.get(pos);
            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
    }
}

//    public interface Callback {
//        void onItemClick(int pos);
//    }

