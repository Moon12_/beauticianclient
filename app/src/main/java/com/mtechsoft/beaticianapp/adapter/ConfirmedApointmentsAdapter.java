package com.mtechsoft.beaticianapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;


public class ConfirmedApointmentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<MemberListModel> memberListModels;

    public ConfirmedApointmentsAdapter(Context context, ArrayList<MemberListModel> memberListModels) {
        this.context = context;
        this.memberListModels = memberListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conferm_appointment_row, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);


    }

    @Override
    public int getItemCount() {
        return memberListModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvAddress, tvTitle;
        ImageView imbPopUp;
        LinearLayout llITem, llShare;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);


        }

        private void bind(int pos) {
            MemberListModel messagesTabModel = memberListModels.get(pos);
//            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
    }
}

//    public interface Callback {
//        void onItemClick(int pos);
//    }

