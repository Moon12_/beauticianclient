package com.mtechsoft.beaticianapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.AppointmentActivity;
import com.mtechsoft.beaticianapp.activites.AvailableSlotsActivity;
import com.mtechsoft.beaticianapp.activites.MemberProfileActivity;
import com.mtechsoft.beaticianapp.dialogFragments.PostDetailFragment;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class StylishPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    boolean isImageFitToScreen;

    private ArrayList<MemberListModel> memberListModels;

    public StylishPostAdapter(Context context, ArrayList<MemberListModel> memberListModels) {
        this.context = context;
        this.memberListModels = memberListModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_posts, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
        holder1.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                PostDetailFragment signupFragment = new PostDetailFragment();
//                signupFragment.show(fm, "Sample Fragment");
//                Log.i(TAG, "click on save button");
//                Toast.makeText(context, "under developing", Toast.LENGTH_SHORT).show();
                showdialogue();
            }
        });

    }

    @Override
    public int getItemCount() {
        return memberListModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvAddress, tvTitle;
        ImageView ivImage;
        LinearLayout llITem, llShare;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage=itemView.findViewById(R.id.ivImage);

        }

        private void bind(int pos) {
            MemberListModel messagesTabModel = memberListModels.get(pos);
//            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
    }
    private void showdialogue() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_post_detail_dialog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.45)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

//        RadioGroup group = dialog.findViewById(R.id.radio);
//        TextInputEditText editText_Name = dialog.findViewById(R.id.edit_Card_Name);
//        TextInputEditText editText_Number = dialog.findViewById(R.id.edit_Card_Number);
//        EditText tvDatePicker = dialog.findViewById(R.id.expeirationDate);
//        TextView tv_Update = dialog.findViewById(R.id.bUpdateCard);
//        tvDatePicker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new SingleDateAndTimePickerDialog.Builder(getActivity())
//                        .bottomSheet()
//                        .curved()
//                        .titleTextColor(Color.RED).backgroundColor(Color.WHITE).mainColor(Color.RED)
////                        .stepSizeMinutes(15)
//                        .displayHours(false)
//                        .displayMinutes(false)
//                        //.todayText("aujourd'hui")
//                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
//                            @Override
//                            public void onDisplayed(SingleDateAndTimePicker picker) {
//                                // Retrieve the SingleDateAndTimePicker
//                            }
////
////                            @Override
////                            public void onClosed(SingleDateAndTimePicker picker) {
////                                // On dialog closed
////                            }
//                        })
//                        .title("Date and Time")
//                        .listener(new SingleDateAndTimePickerDialog.Listener() {
//                            @Override
//                            public void onDateSelected(Date date) {
//
////                                getFormattedDate(date);
//                                tvDatePicker.setText(getFormattedDate(date));
//                            }
//                        }).display();
//            }
//        });
        dialog.show();
    }

}

//    public interface Callback {
//        void onItemClick(int pos);
//    }

