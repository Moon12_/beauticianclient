package com.mtechsoft.beaticianapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.models.HomePagerModel;

import java.util.ArrayList;

public class ImageSliderAdapter extends RecyclerView.Adapter<ImageSliderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HomePagerModel> list;
    ViewPager2 viewPager2;

    public ImageSliderAdapter(Context context, ArrayList<HomePagerModel> list, ViewPager2 viewPager2) {
        this.context = context;
        this.list = list;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.row_today_appointment, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        HomePagerModel model = list.get(position);

        holder.stylist_name.setText(model.getStylist_name());
        holder.appointment_date.setText(model.getAppointment_date());
        holder.appointment_address.setText(model.getAppointment_address());
        holder.appointment_time.setText(model.getAppointment_time());
        holder.getAppointment_contact_no.setText(model.getGetAppointment_contact_no());



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView stylist_name,appointment_date,appointment_time,appointment_address,getAppointment_contact_no;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            stylist_name = itemView.findViewById(R.id.tv_stylist_Name);
            appointment_date = itemView.findViewById(R.id.tv_Appointment_Date);
            appointment_time = itemView.findViewById(R.id.tv_Appointment_Time);
            appointment_address = itemView.findViewById(R.id.tv_Appointment_Address);
            getAppointment_contact_no = itemView.findViewById(R.id.tv_Appointment_Contact);

        }

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            list.addAll(list);
            notifyDataSetChanged();
        }
    };
}


