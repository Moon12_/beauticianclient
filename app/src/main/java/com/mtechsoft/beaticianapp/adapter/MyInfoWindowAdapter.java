package com.mtechsoft.beaticianapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.mtechsoft.beaticianapp.R;

public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    Context context;

    public MyInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = LayoutInflater.from(context).inflate(R.layout.design_map_marker_title, null);
        TextView title = view.findViewById(R.id.tv_address_map);
        title.setText(marker.getTitle());
        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
