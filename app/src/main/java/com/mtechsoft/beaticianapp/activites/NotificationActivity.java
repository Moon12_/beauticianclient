package com.mtechsoft.beaticianapp.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.NotificationAdapter;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private NotificationAdapter pAdapter;
    private ArrayList<MemberListModel> memberListModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        recyclerView=findViewById(R.id.rvMmberList);
        initAdapter();
    }
    private void setPoemsName() {
        memberListModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            MemberListModel allPostsTabModel = new MemberListModel();
//            allPostsTabModel.setUserName("Sheikh" + i);
            allPostsTabModel.setPages(10);
            memberListModels.add(allPostsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new NotificationAdapter(NotificationActivity.this, memberListModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }

    public void backClick(View view) {
        finish();
    }
}