package com.mtechsoft.beaticianapp.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.AvailableSlotsAdapter;
import com.mtechsoft.beaticianapp.adapter.NotificationAdapter;
import com.mtechsoft.beaticianapp.models.AvailableSlotsModel;
import com.mtechsoft.beaticianapp.models.MemberListModel;

import java.util.ArrayList;

public class AvailableSlotsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AvailableSlotsAdapter pAdapter;
    ImageView ivBack;
    private ArrayList<AvailableSlotsModel> availableSlotsModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_slots);
        recyclerView=findViewById(R.id.rvMmberList);
        ivBack=findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initAdapter();
    }

    private void setPoemsName() {
        availableSlotsModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            AvailableSlotsModel allPostsTabModel = new AvailableSlotsModel();
//            allPostsTabModel.setUserName("Sheikh" + i);
            allPostsTabModel.setPages(10);
            availableSlotsModels.add(allPostsTabModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new AvailableSlotsAdapter(AvailableSlotsActivity.this, availableSlotsModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AvailableSlotsActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }

    public void backClick(View view) {
        finish();
    }
}