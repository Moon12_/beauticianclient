package com.mtechsoft.beaticianapp.activites;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.activites.NotificationActivity;
import com.mtechsoft.beaticianapp.activites.Spalsh;

import java.util.ArrayList;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class StylishMainActivity extends AppCompatActivity {
    public NavController navController;
    ImageView noticaitn_ic, ivNotification, logout;
    private DrawerLayout drawer;
    public static ImageView logo;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    TextView tvTitle;
    BottomNavigationView vendor_bottom_navigaitaon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylish_main);
        rlToolbar = findViewById(R.id.rlToolbar);
        ivNotification = findViewById(R.id.noticaitn_ic);
        vendor_bottom_navigaitaon = findViewById(R.id.vendor_bottom_navigaitaon);
        tvTitle = findViewById(R.id.tvTitle);
//        logo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent refresh = new Intent(MainActivity.this, MainActivity.class);
//                startActivity(refresh);
//                finish();
//
//            }
//        });
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StylishMainActivity.this, NotificationActivity.class));
            }
        });
        initNavigation();

    }

    private void initNavigation() {
        navController = Navigation.findNavController(this, R.id.user_container);
        NavigationUI.setupWithNavController(vendor_bottom_navigaitaon, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    tvTitle.setText(destination.getLabel());
                    if (destination.getLabel().equals("ProfileFragment2") || destination.getLabel().equals("VendorProductsFragment") || destination.getLabel().equals("Products") || destination.getLabel().equals("PostAnProductFragmentNew")) {
                        rlToolbar.setVisibility(View.GONE);
//                        tvTitle.setText(destination.getLabel());
                    } else {
                        rlToolbar.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(StylishMainActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.accent)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.accent,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
//                                SessionManager sessionManager = new SessionManager(MainActivity.this);
//                                sessionManager.logoutUser();
//                                Utilities.clearSharedPref(MainActivity.this);
//                                Utilities.saveString(MainActivity.this,"login_status","no");
                                startActivity(new Intent(StylishMainActivity.this, Spalsh.class));
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    @Override
    public void onBackPressed() {
        if (!navController.getCurrentDestination().getLabel().toString().equals("Dashboard")) {
            super.onBackPressed();
        } else {

            showCustomDialogforExit();
        }
    }

    private void showCustomDialogforExit() {
        final PrettyDialog pDialog = new PrettyDialog(StylishMainActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.accent)
                .addButton(
                        "Yes",
                        R.color.accent,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}