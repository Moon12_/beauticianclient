package com.mtechsoft.beaticianapp.activites;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.mtechsoft.beaticianapp.R;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class SettingActivity extends AppCompatActivity {
    MaterialButton logout;
    ImageView ivBack;
    LinearLayout llPassword;
    LinearLayout llprivacy, lltermsCondition,refundPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        logout = findViewById(R.id.logout);
        ivBack = findViewById(R.id.ivBack);
        llPassword = findViewById(R.id.llPassword);
        llprivacy = findViewById(R.id.llprivacy);
        lltermsCondition = findViewById(R.id.lltermsCondition);
        refundPolicy = findViewById(R.id.refundPolicy);
        llprivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.websitepolicies.com/policies/view/6QJ5MRmq"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }); refundPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.websitepolicies.com/policies/view/o9HOm6BZ"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        lltermsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.websitepolicies.com/policies/view/ev9zZDqg"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });
        llPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, ChangePassActivity.class));
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(SettingActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.accent)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.accent,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
//                                SessionManager sessionManager = new SessionManager(SettingActivity.this);
//                                sessionManager.logoutUser();
//                                Utilities.clearSharedPref(SettingActivity.this);
//                                Utilities.saveString(SettingActivity.this,"login_status","no");
                                startActivity(new Intent(SettingActivity.this, Spalsh.class));
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.accent,
                        R.color.white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}