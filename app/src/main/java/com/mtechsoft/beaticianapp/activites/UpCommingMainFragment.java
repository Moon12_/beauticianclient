package com.mtechsoft.beaticianapp.activites;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toolbar;

import com.google.android.material.tabs.TabLayout;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.styllishSides.ClientHistoryAdapter;
import com.mtechsoft.beaticianapp.fragments.stylishFragments.HistoryAppointmentFragment;
import com.mtechsoft.beaticianapp.fragments.stylishFragments.UpCommingAppointmentFragment;

public class UpCommingMainFragment extends Fragment {
View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_up_comming_main, container, false);
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        FrameLayout framLayout = view.findViewById(R.id.framLayout);
        tabLayout.addTab(tabLayout.newTab().setText("UpComming"));
        tabLayout.addTab(tabLayout.newTab().setText("Past"));

        //replace default fragment
        replaceFragment(new UpCommingAppointmentFragment());
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new UpCommingAppointmentFragment());
                } else if (tab.getPosition() == 1) {
                    replaceFragment(new HistoryAppointmentFragment());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return  view;
    }
    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.framLayout, fragment);

        transaction.commit();
    }
}