package com.mtechsoft.beaticianapp.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.Utilities;

public class SignupActivity extends AppCompatActivity {
    TextView bSignup, tvType;
    RelativeLayout layout_SignInText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        bSignup = findViewById(R.id.bSignup);
//        tvType = findViewById(R.id.tvType);
        layout_SignInText = findViewById(R.id.layout_SignInText);
        ImageView ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String loginAs = Utilities.getString(SignupActivity.this, "loginAs");
//        if (loginAs.equals("user")) {
//            tvType.setText("(User)");
//
//        } else if (loginAs.equals("stylish")) {
//            tvType.setText("(Stylish)");
//        }
        bSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
            }
        });
        layout_SignInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
            }
        });
    }
}