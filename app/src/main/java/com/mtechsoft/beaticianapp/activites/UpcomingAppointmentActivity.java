package com.mtechsoft.beaticianapp.activites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.adapter.ImageSliderAdapter;
import com.mtechsoft.beaticianapp.models.HomePagerModel;

import java.util.ArrayList;

public class UpcomingAppointmentActivity extends AppCompatActivity {


    ViewPager2 viewPager2;
    private Handler ImageSliderHandler = new Handler();
    ArrayList<HomePagerModel> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_appointment);


        viewPager2 =findViewById(R.id.viewpager);
        setSlider();
    }

    private void setSlider() {

        //Slider images
        list = new ArrayList<>();
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));
        list.add(new HomePagerModel("Appointment with Stylist Name","November 17","12:30 PM","Main Street, 18","+1 111 111 1111"));

        viewPager2.setAdapter(new ImageSliderAdapter(this, list, viewPager2));
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(2);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {


                float a = 1 - Math.abs(position);
                page.setScaleY(0.85f + a * 0.15f);
            }
        });

        viewPager2.setPageTransformer(compositePageTransformer);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                ImageSliderHandler.removeCallbacks(imagesliderRunable);
                ImageSliderHandler.postDelayed(imagesliderRunable, 3000);
            }


        });

    }
    private Runnable imagesliderRunable = new Runnable() {
        @Override
        public void run() {

            viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
        }
    };
}