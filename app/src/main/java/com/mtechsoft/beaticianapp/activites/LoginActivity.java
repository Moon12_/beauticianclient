package com.mtechsoft.beaticianapp.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.Utilities;

public class LoginActivity extends AppCompatActivity {
    TextView txtRegister;
    MaterialButton btn_Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        txtRegister = findViewById(R.id.txtRegister);
        btn_Login = findViewById(R.id.btnLogin);
        ImageView ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String loginAs= Utilities.getString(LoginActivity.this,"loginAs");
      Utilities.saveString(LoginActivity.this,"getAddress","");

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });
    }

    public void goToResetPass(View view) {
        startActivity(new Intent(LoginActivity.this,ResetpassActivity.class));
    }
}