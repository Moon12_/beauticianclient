package com.mtechsoft.beaticianapp.activites;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.Utilities;

public class TrackServiceProviderActivity extends AppCompatActivity implements OnMapReadyCallback {


    Button buttonChat;
    RelativeLayout layout_back;
    private GoogleMap locationmap;
    FusedLocationProviderClient fusedLocationProviderClient;
    static public Location currenlocation;
    private static final int REQUEST_CODE = 103;
    String lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_on_map);
        layout_back = findViewById(R.id.layout_back);
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            lat = String.valueOf(gpsTracker.getLatitude());
            lng = String.valueOf(gpsTracker.getLongitude());
//
//            Utilities.saveString(LocationActivity.this, "latitude_near", latitude);
//            Utilities.saveString(LocationActivity.this, "longitude_near", longitude);

//            Toast.makeText(HomeActivity.this, latitude + " " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gpsTracker.showSettingsAlert();
        }
//        lat = Utilities.getString(TrackServiceProviderActivity.this, "new_latitude");
//        lng = Utilities.getString(TrackServiceProviderActivity.this, "new_longitude");

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(TrackServiceProviderActivity.this);
        fetchLastLxocation();
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void fetchLastLxocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            ;


            return;
        }


        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {

                    currenlocation = location;
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googlemap);
                    supportMapFragment.getMapAsync(TrackServiceProviderActivity.this);
                }

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        locationmap = googleMap;
        LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        markerOptions.title("Here");
        locationmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        locationmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        locationmap.addMarker(markerOptions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    fetchLastLxocation();
                }
                break;
        }
    }
}