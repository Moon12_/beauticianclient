package com.mtechsoft.beaticianapp.activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.mtechsoft.beaticianapp.R;
import com.mtechsoft.beaticianapp.Utilities;

public class WelcomeActivity extends AppCompatActivity {

    //    Button buttonLogin;
    RelativeLayout RLsignupTxt;
    LinearLayout buttonSignUp, buttonSignUpProvider;
    CardView card_user, cardStylish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);

//        buttonLogin = (Button) findViewById(R.id.loginButton);
        buttonSignUp = findViewById(R.id.imageViewFirstOption);
        buttonSignUpProvider = findViewById(R.id.imageViewSecondOption);
//        RLsignupTxt = findViewById(R.id.RLsignupTxt);
        card_user = findViewById(R.id.card_user);
        cardStylish = findViewById(R.id.cardStylish);
//
//        buttonLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
//                startActivity(intent);
//            }
//        });
        cardStylish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, SignupActivity.class);
                Utilities.saveString(WelcomeActivity.this, "loginAs", "stylish");
                startActivity(intent);
            }
        });
        card_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.saveString(WelcomeActivity.this, "loginAs", "user");

                Intent intent = new Intent(WelcomeActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

//        buttonSignUp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Sign up as Buyer
//                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
//                startActivity(intent);
//
//            }
//        });

//        RLsignupTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(WelcomeActivity.this, SignupActivity.class));
//            }
//        });

    }

    public void backClick(View view) {
        finish();
    }
}
