package com.mtechsoft.beaticianapp.dialogFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mtechsoft.beaticianapp.R;

public class PostDetailFragment extends BottomSheetDialogFragment {
    public static final String TAG = "ActionBottomDialog";
//    private ItemClickListener mListener;

    View view;

    public static PostDetailFragment newInstance() {
        return new PostDetailFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_post_detail_dialog, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof ItemClickListener) {
//            mListener = (ItemClickListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement ItemClickListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

//    @Override
//    public void onClick(View view) {
//        TextView tvSelected = (TextView) view;
//        mListener.onItemClick(tvSelected.getText().toString());
//        dismiss();
//    }

    public interface ItemClickListener {
        void onItemClick(String item);
    }



}