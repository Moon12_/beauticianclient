package com.mtechsoft.beaticianapp.models;

public class AvailableSlotsModel {
  int pages;
  String userName;
  String chapterDetail;

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getChapterDetail() {
    return chapterDetail;
  }

  public void setChapterDetail(String chapterDetail) {
    this.chapterDetail = chapterDetail;
  }
}

