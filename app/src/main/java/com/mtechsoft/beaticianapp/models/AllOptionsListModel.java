package com.mtechsoft.beaticianapp.models;

public class AllOptionsListModel {
  String title;

  public AllOptionsListModel(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}

