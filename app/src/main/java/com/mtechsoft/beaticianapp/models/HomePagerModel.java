package com.mtechsoft.beaticianapp.models;

public class HomePagerModel {
    String stylist_name,appointment_date,appointment_time,appointment_address,getAppointment_contact_no;

    public HomePagerModel(String stylist_name, String appointment_date, String appointment_time, String appointment_address, String getAppointment_contact_no) {
        this.stylist_name = stylist_name;
        this.appointment_date = appointment_date;
        this.appointment_time = appointment_time;
        this.appointment_address = appointment_address;
        this.getAppointment_contact_no = getAppointment_contact_no;
    }

    public HomePagerModel() {
    }

    public String getStylist_name() {
        return stylist_name;
    }

    public void setStylist_name(String stylist_name) {
        this.stylist_name = stylist_name;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getAppointment_address() {
        return appointment_address;
    }

    public void setAppointment_address(String appointment_address) {
        this.appointment_address = appointment_address;
    }

    public String getGetAppointment_contact_no() {
        return getAppointment_contact_no;
    }

    public void setGetAppointment_contact_no(String getAppointment_contact_no) {
        this.getAppointment_contact_no = getAppointment_contact_no;
    }
}
